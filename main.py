import petstore_app
import weather_app

menu_pl = """
===========MENU===========
1. Aplikacja pogodowa
2. Sklep zoologiczny
==========================
"""
menu_en = """
===========MENU===========
1. Weather APP
2. PetStore
==========================
"""
def menu():
    print("========================")
    print("1. Polski")
    print("2. English")
    print("========================")    
    
    choise = 0
    try:
        choise = int(input())
    except:
        print("Podano zły język. Incorrect lang")
        menu()    
    
    lang = ""
    if choise == 1:
        lang = "pl"
        print(menu_pl)    
    elif choise == 2:
        lang = "en"
        print(menu_en)
    else:
        print("Podano zły język. Incorrect lang")
        menu()    

    try:
        choise = int(input())
    except:
        print("Wpisano nieprawidłową wartość")
        menu()   
    
    if choise == 1:
        weather_app.run(lang)
    elif choise == 2:
        petstore_app.run()
    else:
        print("podano nieprawidłową wartość\n")    
        menu()
        
menu()