import requests

choose_temp_unit_pl = "Wybierz jednostkę temperetury: "
choose_temp_unit_en = "Choose temperature unit: "

wrong_temp_unit_pl = "Wybrano nieprawidłową jednostke temperatury"
wrong_temp_unit_en = "You choose wrong temperature unit"

choose_city_pl = "Wybierz miasto: "
choose_city_en = "Choose city: "

wrong_city_pl = "Podano nieprawidłową nazwę miasta"
wrong_city_en = "Wrong city name"

city_not_found_pl = "Nie znaleziono miasta"
city_not_found_en = "City not found"

def run(lang):
    print("\nUruchomiono aplikacje pogodową\n")

    if lang == "pl":
        print(choose_temp_unit_pl)
    else:
        print(choose_temp_unit_en)

    print("Wybierz jednostkę temperatury")
    print("1. \u2103") #unicode stopnie w C (więcej na fileformat)
    print("2. \u2109") #unicode stopnie w F
    temp_unit = 0
    units = ""
    try:
        temp_unit = int(input())
    except:
        if lang == "pl":
            print(wrong_temp_unit_pl)
        else:
            print(wrong_temp_unit_en)
        run(lang)

    units_description = ""

    if temp_unit == 1:
        units = "metric"
        units_description = "C"
    else:
        units = "imperial"
        units_description = "F"

    if lang == "pl":
        print(choose_city_pl)
    else:
        print(choose_city_en)

    city = input()

    params = { #query params
        "appid": "7d2c995b0ecd49024c993ff407ae6379",
        "q": city,
        "lang": lang,
        "units": units
    }
    url = "https://api.openweathermap.org/data/2.5/weather"

    response = requests.get(url, params)
    if(response.status_code == 400):
        if lang == "pl":
            print(wrong_city_pl)
        else:
            print(wrong_city_en)
        run(lang)
    elif(response.status_code == 404):
        if lang == "pl":
            print(city_not_found_pl)
        else:
            print(city_not_found_en)
        run(lang)
    res = response.json()

    print("Nazwa miasta: " + res["name"])
    print("Pogoda: " + res["weather"][0]["description"])
    print("Temperatura: " + str(res["main"]["temp"]) + " \u0366" + units_description)
    print("Temperatura min: " + str(res["main"]["temp_min"]) + " \u0366" + units_description)
    print("Temperatura max: " + str(res["main"]["temp_max"]) + " \u0366" + units_description)
    print("Temperatura oczuwalna: " + str(res["main"]["feels_like"]) + " \u0366" + units_description)
    exit(0)



