import request

def run():
    print("Uruchomiono aplikacje petstore\n")
    print("""
    =================MENU=================
    1. Utwórz zwierzaka
    2. Zaktualizuj zwierzaka
    """)
    choise = 0
    try:
        choise == int(input())
    except:
        print("Błędny wybór. Spróbuj ponownie")
        run()
    if choise == 1:
        create_pet()
    elif choise ==2:
        update_pet()

def create_pet():
    print("Podaj imię zwierzaka: ")
    name = input()

    url = "https://petstore.swagger.io/v2/pet"

    body = { #słownik w jsonie
        "name": name
    }

    response = request.post(url, json=body)
    id = 0
    if response.status_code == 200:
        id = response.json()["id"]
        print("Utworzono zwierzaka")
        print("ID: " + str(id) + " imię: " + name)
    else:
        print("Nie udało się utworzyć zwierzaka")
    run()

def update_pet():
    print("Podaj id zwierzaka")
    id = input()

    print("Podaj nową nazwę zwierzaka: ")
    name = input()

        url = "https://petstore.swagger.io/v2/pet"

    body = { #słownik w jsonie
        "id": id,
        "name": name
    }

    response = request.post(url, json=body)
    print(response)
    print(response.json())